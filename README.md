# ansible-production-setup

## 本番環境作成

### VirtualBox（`vagrant`）の場合

  1. 仮想環境起動

      ```bash
      vagrant up
      ```

  2. オンライン環境におけるAnsibleプレイブック実行

      ```bash
      ansible-playbook site.yml --tags=online
      ```

  3. オフライン環境におけるAnsibleプレイブック実行

      ```bash
      ansible-playbook site.yml --tags=<各種role>
      ```

  4. ローカル環境に対する実行

     ```bash
     ansible-playbook -i inventories/hosts site.yml
     ```

### dockerコンテナの場合

  1. dockerコンテナの作成

     ```bash
     git clone https://gitlab.com/y-ok/docker-ansible-practice.git
     cd docker-ansible-practice
     docker-compose up -d --build
     ```

  2. playbookの配備

     ```bash
     cd playbooks
     git clone https://gitlab.com/y-ok/ansible-production-setup.git
     ```

  3. ansible実行環境へログイン

      ```bash
      docker exec -it controller /bin/bash
      ```

  4. dockerコンテナに対する実行

     ```bash
     ansible-playbook -i inventories/docker-hosts site.yml -e ansible_user="ansible" -e ansible_ssh_private_key_file="/root/.ssh/ansible_rsa"
     ```

## 各種roleの説明

### role: `online`

オンライン環境におけるパッケージインストールし、プライベートリポジトリを作成する。

- `yum_packages_install.sh`の配備
- `yum_packages_install.sh`の実行
  - パッケージのローカルインストール
  - プライベートリポジトリ作成
- `yum_packages_install.sh`の削除

### role: `common`

  各種COTSインストール時に必要となるパッケージをインストールする。

| Package                 | Description                                                                                                                                                           |
|-------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| cifs-utils              | The cifs-utils provides a means for mounting SMB/CIFS shares on a Linux system                                                                                        |
| curl-devel              | Header files and static libraries for libcurl                                                                                                                         |
| expat-devel             | Libraries and header files to develop applications using expat                                                                                                        |
| gettext-devel           | Development files for gettext                                                                                                                                         |
| openssl-devel           | Development files needed to develop applications which support various cryptographic algorithms and protocols                                                         |
| perl-devel              | Perl header files and development modules                                                                                                                             |
| zlib-devel              | Header files and libraries for Zlib development                                                                                                                       |
| perl-ExtUtils-MakeMaker | Create a module Makefile                                                                                                                                              |
| wget                    | Package for retrieving files using HTTP, HTTPS, FTP and FTPS the most widely-used Internet protocols                                                                  |
| mlocate                 | Merging locate and database package                                                                                                                                   |
| lsof                    | Command in Linux displays in its output information about files that are opened by processes                                                                          |
| tree                    | Recursive directory listing program that produces a depth-indented listing of files                                                                                   |
| htop                    | An interactive process viewer for Unix systems                                                                                                                        |
| jq                      | A lightweight and flexible command-line JSON processor                                                                                                                |
| pigz                    | Parallel implementation of gzip                                                                                                                                       |
| lbzip2                  | Parallel bzip2 utility                                                                                                                                                |
| pbzip2                  | Parallel bzip2 file compressor                                                                                                                                        |
| pxz                     | Parallel LZMA compressor compatible with XZ                                                                                                                           |
| bzip2-devel             | Libraries and header files for apps which will use bzip2                                                                                                              |
| gdbm-devel              | Development libraries and header files for the gdbm library                                                                                                           |
| libffi-devel            | Header files and library for Foreign Function Interface development                                                                                                   |
| libuuid-devel           | Universally unique ID library                                                                                                                                         |
| ncurses-devel           | Development files for the ncurses library                                                                                                                             |
| readline-devel          | A set of functions that allow users to edit typed command lines                                                                                                       |
| sqlite-devel            | An Embeddable SQL Database Engine                                                                                                                                     |
| tk-devel                | Tk graphical toolkit development files                                                                                                                                |
| xz-devel                | Devel libraries & headers for liblzma                                                                                                                                 |
| gcc-c++                 | C++ support to the GNU Compiler Collection                                                                                                                            |
| automake                | Generate Makefile.in for configure from Makefile.am                                                                                                                   |
| gcc                     | A standard conforming and highly portable ISO C and ISO C++ compiler                                                                                                  |
| libtool                 | A generic library support script                                                                                                                                      |
| zip                     | A compression and file packaging utility                                                                                                                              |
| unzip                   | A list, test, or extraction files from a zip archive                                                                                                                  |
| vim-enhanced            | An updated and improved version of the vi editor                                                                                                                      |
| tcsh                    | The Tcsh package contains an enhanced but completely compatible version of the Berkeley Unix C shell                                                                  |
| samba                   | Samba is the standard Windows interoperability suite of programs for Linux and Unix.                                                                                  |
| samba-client            | Samba-client provides some SMB clients, which complement the built-in SMB filesystem in Linux. These allow the accessing of SMB shares, and printing to SMB printers. |
| make                    | GNU Make is a tool which controls the generation of executables and other non-source files of a program from the program's source files.                              |
| libaio                  | The libaio package is an asynchronous I/O facility ("async I/O", or "aio") that has a richer API and capability set than the simple POSIX async I/O facility.         |

### role: `user`

  Linuxのグループ、ユーザを作成する。

- グループ作成
- ユーザー作成

### role: `oralce`

　構築内容は以下の通り。

- ライブラリインストール
- カーネルパラメータ設定
- グループ作成
- ユーザ作成
- インストールディレクトリ作成
- リソース制限の設定
- 環境変数設定
- インストーラー配備、インストール実施
- レスポンスファイルを利用したOracleデータベースインストール
- DBCAを利用したデータベース作成
- tnsnames.ora配備
- 表領域の作成
  - 表領域を使用できるサイズは無制限
- データベース利用ユーザ作成、権限付与
- データベース利用ユーザのパスワード無効化

#### 環境変数

| Environment variables | Path                                    |
|-----------------------|-----------------------------------------|
| ORACLE_BASE           | /u01/app/oracle                         |
| ORACLE_HOME           | /u01/app/oracle/product/12.2.0/dbhome_1 |
| LANG                  | ja_JP.UTF-8                             |
| NLS_LANG              | Japanese_Japan.AL32UTF8                 |

#### Oracleインストール先ディレクトリ構成

```bash
/u01/app/oracle/
├── admin
│   ├── USER01OPE1
│   │   ├── adump
│   │   ├── dpdump
│   │   ├── pfile
│   │   └── xdb_wallet
│   ├── USER01OPE2
│   └── USER01OPE3
├── audit
│   ├── USER01OPE1
│   ├── USER01OPE2
│   └── USER01OPE3
├── cfgtoollogs
│   ├── dbca
│   │   ├── USER01OPE1
│   │   ├── USER01OPE2
│   │   ├── USER01OPE3
│   │   ├── trace.log_2020-07-26_07-59-19-AM
│   │   ├── trace.log_2020-07-26_08-02-25-AM
│   │   └── trace.log_2020-07-26_08-05-19-AM
│   └── sqlpatch
│       ├── sqlpatch_24161_2020_07_26_08_01_18
│       ├── sqlpatch_25902_2020_07_26_08_04_16
│       ├── sqlpatch_27683_2020_07_26_08_07_24
│       └── sqlpatch_history.txt
├── checkpoints
├── diag
│   ├── afdboot
│   ├── apx
│   ├── asm
│   ├── asmtool
│   ├── bdsql
│   ├── clients
│   ├── crs
│   ├── diagtool
│   ├── dps
│   ├── em
│   ├── gsm
│   ├── ios
│   ├── lsnrctl
│   ├── netcman
│   ├── ofm
│   ├── plsql
│   ├── plsqlapp
│   ├── rdbms
│   │   ├── user01ope1
│   │   ├── user01ope2
│   │   └── user01ope3
│   └── tnslsnr
└── product
    └── 12.2.0
        └── dbhome_1
```

#### データ領域のディレクトリ構造

```bash
/home/oracle/
├── oradata
│   ├── USER01OPE1
│   │   ├── control01.ctl
│   │   ├── control02.ctl
│   │   ├── redo01.log
│   │   ├── redo02.log
│   │   ├── redo03.log
│   │   ├── sysaux01.dbf
│   │   ├── system01.dbf
│   │   ├── temp01.dbf
│   │   ├── undotbs01.dbf
│   │   ├── USER01OPE1_DATATABLE.dbf
│   │   └── users01.dbf
│   ├── USER01OPE2
│   └── USER01OPE3
├── oraInventory
│   ├── ContentsXML
│   ├── logs
│   ├── oraInst.loc
│   ├── orainstRoot.sh
│   └── oui
└── script
    ├── db_setup_user01_USER01OPE1.sh
    ├── db_setup_user01_USER01OPE2.sh
    ├── db_setup_user01_USER01OPE3.sh
    ├── ORACLE_LSNRCTL_START.sh
    └── ORACLE_LSNRCTL_STOP.sh
```

### role: `git`

- 古いバージョンの`git`を削除
- `git-2.28.0.tar.gz`の展開
- ビルド、インストール
- 不要ファイル、フォルダの削除
- `git`バイナリファイルのシンボリックリンク作成

### role: `java`

- `javaインストーラ`のコピー
- yumコマンドによるインストール
- 不要ファイルの削除

### role: `maven`

- `maven`の配備
- シンボリックリンク作成
- `/etc/profile`へのパス設定

### role: `cppunit`

- cppunitの展開
- Makefileの作成
- ビルド、インストール
- `/etc/ld.so.conf`への設定反映

### role: `xerces-c`

- `xerces-c`、`icu`の展開
- Makefileの作成
- ビルド、インストール
- 展開ディレクトリの削除

## 参考

- `https://www.oracle.com/technetwork/jp/database/enterprise-edition/documentation/sidb12201-inst-linux-x64-ja-v10-3627443-ja.pdf`
- `https://oracledbwr.com/oracle-automation-oracle-database-creation-using-ansible-tool/`
- `http://yskwkzhr.blogspot.com/2017/04/setup-oracle-database-122-using-vagrant.html`
- `https://github.com/shakiyam/vagrant-oracle12.2`
- `https://www.nodalpoint.com/devops-ansible-oracle-database-oraclelinux-7-vagrant/`
- `https://qiita.com/s-sasaki/items/cb768bd00d3588f494d4`
- `https://ts0818.hatenablog.com/entry/2018/03/18/161320`
- `https://github.com/ellotheth/ansible-oracle`
- `https://www.programering.com/a/MzN5QjMwATk.html`
- `https://www.coppermine.jp/docs/notepad/2013/12/oracle-database-12c.html`
- `https://github.com/ChristopherDavenport/ansible-role-oracle-client`
- `https://blog.payara.fish/fine-tuning-payara-server-in-production-japanese`
- `https://www.payara.fish/payara-site/media/gb/Payara-Server-4-to-5-Migration-Guide-Japanese.pdf`
- `https://www.coppermine.jp/docs/notepad/2017/12/payara-micro-internals-2017.html`
- `https://www.coppermine.jp/docs/notepad/2017/06/how-to-use-payara-micro-maven-plugin-ja.html`
- `https://docs.payara.fish/documentation/payara-micro/appendices/cmd-line-opts.html`
- `https://docs.payara.fish/documentation/payara-micro/configuring/config-keystores.html`
